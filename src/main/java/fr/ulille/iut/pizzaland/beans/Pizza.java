package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private List<Ingredient> ingredients;
	private long id;
	private String name;

	public Pizza() {
	}

	public Pizza(List<Ingredient> ingredients, long id, String name) {
		this.ingredients = ingredients;
		this.id = id;
		this.name = name;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setIngredients(p.getIngredients());
		dto.setId(p.getId());
		dto.setName(p.getName());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setIngredients(dto.getIngredients());
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());

		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (ingredients != other.ingredients)
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pizza [id = " + id + ", name = " + name + ", ingredients = " + ingredients + "]";
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setIngredients(pizza.getIngredients());
		dto.setName(pizza.getName());
		
		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setIngredients(dto.getIngredients());
		pizza.setName(dto.getName());

		return pizza;
	}
}
