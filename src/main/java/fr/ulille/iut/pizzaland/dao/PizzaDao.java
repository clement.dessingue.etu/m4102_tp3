package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	static IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzasAndIngredients (pizza INTEGER,"
			+ " ingredient INTEGER,"
			+ " PRIMARY KEY(pizza, ingredient),"
			+ " FOREIGN KEY pizza REFERENCES pizzas(id),"
			+ " FOREIGN KEY ingredient REFERENCES ingredients(id))")
	void createAssociationTable();

	default void createTable() {
		createPizzaTable();
		createAssociationTable();
	}
	
	@SqlQuery("SELECT ingredient FROM pizzasAndIngredients WHERE pizza = :pizzaId")
	long getId(long pizzaId);

	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropTable();

	@SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insertPizza(String name);
	
	@SqlUpdate("INSERT INTO pizzasAndIngredients VALUES (:pizza, :ingredient")
	public void associatePizzaWithIngredient(long pizza, long ingredient);
	
	public default long insert(String name, List<Ingredient> ingredients) {
		long pizzaId = insertPizza(name);
		
		for(Ingredient ingredient : ingredients)
			associatePizzaWithIngredient(pizzaId, ingredient.getId());
		return pizzaId;
	}

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	default List<Pizza> getAllPizzas() {
		List<Pizza> pizzas = getAll();

		for (Pizza pizza : pizzas)
			pizza.setIngredients(ingredientDao.getAllIngredientsOfPizza(getId(pizza.getId())));

		return pizzas;
	}

	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);

	default Pizza getPizzaById(long id) {
		Pizza pizza = findById(id);
		pizza.setIngredients(ingredientDao.getAllIngredientsOfPizza(id));
		return pizza;
	}

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);

	@SqlUpdate("DELETE FROM pizzasAndIngredient WHERE pizza = :id")
	void removePAI(long id);

	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void removePizza(long id);

	default void remove(long id) {
		removePAI(id);
		removePizza(id);
	}
}
