1d) créer un ingrédient :
curl -i --noproxy localhost -H "Con" --data-raw '{"name":"saucisse"}' localhost:8080/api/v1/ingredients

récupérer la liste des ingrédients :
curl -i --noproxy localhost localhost:8080/api/v1/ingredients

récupérer l'ingrédient 13 par exemple :
curl -i --noproxy localhost localhost:8080/api/v1/ingredients/13

détruire l'ingrédient 13 :
curl -i --noproxy localhost -X "DELETE" localhost:8080/api/v1/ingredients/13